package at.jku.prbs.procexp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;

import at.jku.prbs.procexp.data.ProcessInformation;

public class ProcessDataManager {

    private HashMap<Integer, ProcessInformation> allProcessesInfo;
    private long cpuPreviousTicks = 0;
    
    /**
     * Returns the current array of all process information objects.
     *  
     * @return Array of process information objects.
     */
    public ProcessInformation[] getAllProcessesInfo() {
		if (allProcessesInfo == null) { return null; }

        return allProcessesInfo.values().toArray(new ProcessInformation[0]);
    }

    /**
     * Update the current array of process information objects and store it.
     * 
     * @return Array of process information objects.
     */
    public ProcessInformation[] updateCurrentProcessInformation() {
    	return updateCurrentProcessInformation(null);
    }

    /**
     * Update the current array of process information objects and store it.
     * 
	 * @param procDirectory
	 *            The path to the procfs. If null, the default path is taken.
     * @return Array of process information objects.
     */
    public ProcessInformation[] updateCurrentProcessInformation(Path procDirectory) {
   		allProcessesInfo = ProcessParser.parseProcessDetails(procDirectory, allProcessesInfo);

   		computeCPUUsage(procDirectory);
   		
        return getAllProcessesInfo();
    }
    
    
    /**
     * Compute the CPU usage in percent for every process.
     * 
     * cpuUsage_p = 100 * (processCPUTicksSinceLastCheck / overallCPUTicksSinceLastCheck)
     * 
	 * @param procDirectory
	 *            The path to the procfs. If null, the default path is taken.
     */
    private void computeCPUUsage(Path procDirectory) {
    	// Get overall CPU ticks
    	long cpuTicks = ProcessParser.parseCPUOverallTicks(procDirectory);
    	
    	long delta = cpuTicks - cpuPreviousTicks;
    	cpuPreviousTicks = cpuTicks;
    	
		if (allProcessesInfo == null) { return; }

    	for (ProcessInformation processInformation : allProcessesInfo.values()) {
			double processTicksDiff = (double)processInformation.getClockTicksIncrease();
			
			if (delta == 0L) { // unexpected, but just to be sure 
				processInformation.setCPUUsage(0.0);
			} else {
				processInformation.setCPUUsage(100.0 * processTicksDiff / (double)delta);
			}
		}
    }
    
    /**
     * Store all process details in the given file.
     * 
     * @param path Path to the file where the information should be stored.
     */
    public void storeProcessInformationToFile(Path path) {
		// ----------------------------------------------------------------------
		// TODO: Code

		StringBuilder temp = new StringBuilder();
		allProcessesInfo
				.entrySet()
				.stream()
				.forEach(a -> temp.append(a).append("\n"));
		String info = temp.toString();

		if (path.toFile().exists()){
			try {
				Files.write(path,info.getBytes(), StandardOpenOption.TRUNCATE_EXISTING);
			} catch (IOException e) {
				System.out.println("ERROR: at storeProcessInformationToFile file overwrite --> File.write() failed");
				e.printStackTrace();
			}
		}else{
			try {
				Files.write(path,info.getBytes(), StandardOpenOption.CREATE);
			} catch (IOException e) {
				System.out.println("ERROR: at storeProcessInformationToFile file create --> File.write() failed");
				e.printStackTrace();
			}
		}
		// ----------------------------------------------------------------------
    }
}
