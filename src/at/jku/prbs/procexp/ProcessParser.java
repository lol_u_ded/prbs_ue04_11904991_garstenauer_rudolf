package at.jku.prbs.procexp;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import at.jku.prbs.procexp.data.ProcessInformation;

import javax.annotation.processing.AbstractProcessor;

/**
 * Reads the process information from the /proc FS and generates an Array with
 * all details
 * 
 * @See https://www.kernel.org/doc/Documentation/filesystems/proc.txt
 */
public class ProcessParser {

	/**
	 * Default location of the /proc file system
	 */
	public final static String DEFAULT_PROCESS_DIRECTORY_PATH = "/proc";

	/*
	 * Enumeration of the index of certain information in the /proc/[pid]/stat
	 * file
	 * 
	 * @see http://man7.org/linux/man-pages/man5/proc.5.html
	 */
	public enum PROC_STAT_FIELDS {
		PID(0),
		PROCESSNAME(1),
		STATE(2),
		UTIME(13),
		STIME(14),
		CUTIME(15),
		CSTIME(16),
		VSIZE(22),
		;;

		private final int index;

		PROC_STAT_FIELDS(int index) { this.index = index; }

		public int value() { return index; }
	}

	/**
	 * Verify that the given process directory path is a valid directory.
	 * 
	 * Otherwise return the default procfs path.
	 * 
	 * @param procDirectory A path to test.
	 * @return Verified path to the procfs.
	 */
	private static Path verifyProcFSPath(Path procDirectory) {
		if ((procDirectory != null) && Files.isDirectory(procDirectory)) {
			return procDirectory;
		} else {
			// fallback to default procfs path
			return Paths.get(DEFAULT_PROCESS_DIRECTORY_PATH);
		}
	}
	
	/**
	 * Parses the /proc file system and updates the hashmap with all
	 * {@link ProcessInformation} objects.
	 * 
	 * @param procDirectory
	 *            The path to the procfs. If null, the default path is taken.
	 * @param previousMap
	 *            Hashmap with {@link ProcessInformation} objects to incorporate
	 *            the previous state.
	 * @return Updated hashmap with all current {@link ProcessInformation}
	 *         objects.
	 */
	public static HashMap<Integer, ProcessInformation> parseProcessDetails(
			Path procDirectory,
			HashMap<Integer, ProcessInformation> previousMap) {
		procDirectory = verifyProcFSPath(procDirectory);
		
		// get fresh list of currently active processes
		Path[] pidPaths = getAllProcessIDPaths(procDirectory);

		HashMap<Integer, ProcessInformation> newProcesses = null;
		if ((pidPaths != null) && (pidPaths.length > 0)) {
			newProcesses = new HashMap<Integer, ProcessInformation>();

			for (Path processInformationPath : pidPaths) {
				// try to find existing process information
				ProcessInformation info = null;
				try {
					int pid = getPIDFromPath(processInformationPath);

					if ((previousMap != null) && (previousMap.containsKey(pid))) {
						info = previousMap.get(pid);
					} else {
						info = new ProcessInformation(pid, processInformationPath);
					}

					updateProcessInformation(info);

					newProcesses.put(pid, info);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return newProcesses;
	}

	/**
	 * Returns the list of process information directories currently available
	 * in the procfs. Each process has its own directory with the PID as its
	 * name. Thus, the PIDs can simply be extracted from the directory listing
	 * of /proc using the following rules:
	 *   - Test if directory entry is a directory.
	 *   - Test if directory entry is a numeric value.
	 * If both conditions are true, the directory entry is a process
	 * information directory.
	 * 
	 * @param procDirectory
	 *            The path to the procfs. If null, the default path is taken.
	 * @return Array of all process information directories.
	 */
	public static Path[] getAllProcessIDPaths(Path procDirectory) {
		procDirectory = verifyProcFSPath(procDirectory);
		ArrayList<Path> pidDirs = new ArrayList<>();
		// ----------------------------------------------------------------------
		// TODO Code

		try {
			DirectoryStream <Path> directoryStream = Files.newDirectoryStream(procDirectory);
			for(Path p :directoryStream ){
				//  Only directory entries that consists of only digits are valid process directories.
				if( getPIDFromPath(p) >= 0){
					pidDirs.add(p);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("ERROR : at DirectoryStream");
		}
		// ----------------------------------------------------------------------

		return pidDirs.toArray(new Path[0]);
	}
	
	/**
	 * Parses and returns the PID based on the process file path.
	 * 
	 * @param processInformationPath
	 *            Path to a /proc/[pid]/ directory for a process.
	 * @return The PID of the given process.
	 */
	private static int getPIDFromPath(Path processInformationPath) {
		String name = processInformationPath.getFileName().toString();
		if (name.matches("^[0-9]+$")) {
			try {
				return Integer.parseInt(name);
			} catch (NumberFormatException ignored) {}
		}
		// return an invalid PID if the path does not
		// end with a valid numeric PID
		return -1;
	}
	
	/**
	 * Function to update the given process.
	 * 
	 * @param processInfo
	 *            Reference to the {@link ProcessInformation} object that should
	 *            be updated.
	 * @throws IOException
	 */
	private static void updateProcessInformation(ProcessInformation processInfo) throws IOException {
		readProcessStatusInformation(processInfo);
		readLinks(processInfo);
		readMemoryAndCPUUsage(processInfo);
	}
	
	/**
	 * Helper function to read general status information of the process
	 * (name, state, UID, PPID) from /proc/[pid]/status.
	 * 
	 * @param processInfo
	 *            Reference to the {@link ProcessInformation} object that should
	 *            be updated.
	 * @throws IOException
	 */
	private static void readProcessStatusInformation(ProcessInformation processInfo) throws IOException {
		// ----------------------------------------------------------------------
		// TODO Code

		Path path = processInfo.getProcessInformationDirPath();
		path = Paths.get(path.toString(), "/status");
		List<String> inputFile = Files.readAllLines(path);
		try {
			inputFile.stream().map(line -> line.split(":\\s+")).forEach(strings -> {

				String compare = strings[0].toLowerCase();

				switch (compare) {
					case "name":
						processInfo.setProcessName(strings[1]);

						break;
					case "uid":
						String[] secondSplit = strings[1].split("\\s+");
						processInfo.setOwner(Integer.parseInt(secondSplit[0]));

						break;
					case "ppid":
						processInfo.setParentProcessID(Integer.parseInt(strings[1]));

						break;
					case "state":
						processInfo.setStatus(strings[1]);
						break;
				}
			});
		} catch (NumberFormatException e) {
			System.out.println("ERROR: at readProcessStatusInformation --> String to int failed");
			e.printStackTrace();
		}
	}

	/**
	 * Helper function to read and resolve the links for the executable file
	 * (/proc/[pid]/exe) and the current working directory (/proc/[pid]/cwd)
	 * of the process.
	 * 
	 * @param processInfo
	 *            Reference to the {@link ProcessInformation} object that should
	 *            be updated.
	 * @throws IOException
	 */
	private static void readLinks(ProcessInformation processInfo) throws IOException {
		// ----------------------------------------------------------------------
		// TODO: Prepare the Path <procfs>/<pid>/cwd, bring it into a canonical
		//       form and store it into the processInfo.
		// ----------------------------------------------------------------------
		String cCWD = processInfo
				.getProcessInformationDirPath()
				.resolve("./cwd")
				.toRealPath()
				.toString();
		processInfo.setCWD(String.valueOf(new File(cCWD).getCanonicalFile()));
		// ----------------------------------------------------------------------
		// TODO: Prepare the Path <procfs>/<pid>/exe, bring it into a canonical
		//       form and store it into the processInfo.
		// ----------------------------------------------------------------------
		String cExe = processInfo
				.getProcessInformationDirPath()
				.resolve("./exe")
				.toRealPath()
				.toString();
		processInfo.setCWD(String.valueOf(new File(cExe).getCanonicalFile()));
		// ----------------------------------------------------------------------
	}

	/**
	 * Helper function to read the memory and CPU usage from /proc/[pid]/stat.
	 * 
	 * @param processInfo
	 *            Reference to the {@link ProcessInformation} object that should
	 *            be updated
	 * @throws IOException
	 */
	private static void readMemoryAndCPUUsage(ProcessInformation processInfo) throws IOException {
		// ----------------------------------------------------------------------
		// TODO Code

		Path path = Paths.get(processInfo.getProcessInformationDirPath().toString(), "stat");
		long kernelTime = 0;
		long userTime = 0;

		try {
			String text = String.valueOf(Files
					.readAllLines(path)
					.stream()
					.findFirst());

			String[] splitArr = text.split("\\s+");

			int i = 0;
			while (i < splitArr.length) {
				if (i == PROC_STAT_FIELDS.VSIZE.value()) {
					processInfo.setAllocatedMemory(Long.parseLong(splitArr[i]));
				}
				if (i == PROC_STAT_FIELDS.STIME.value()) {
					kernelTime = Long.parseLong(splitArr[i]);
				}
				if (i == PROC_STAT_FIELDS.UTIME.value()) {
					userTime = Long.parseLong(splitArr[i]);
				}
				i++;
			}
			processInfo.setOverallClockTicks(kernelTime+userTime);

		} catch (IOException e) {
			System.out.println("Error: at readMemoryAndCPUUsage --> Unable to find file.");
			e.printStackTrace();
		} catch (NumberFormatException e) {
			System.out.println("Error: at readMemoryAndCPUUsage -->unable to convert String to Long.");
			e.printStackTrace();

			// ----------------------------------------------------------------------
		}
	}

	
	/**
	 * Function to read the /proc/stat file and parse the overall CPU ticks
	 * since last boot.
	 * 
	 * @param procDirectory
	 *            The path to the procfs. If null, the default path is taken.
	 * @return Overall CPU ticks since last system boot.
	 * @throws IOException
	 */
	public static long parseCPUOverallTicks(Path procDirectory){
		procDirectory = verifyProcFSPath(procDirectory);

		long uptimeClockTicks = 0;
		
		// ----------------------------------------------------------------------
		// TODO Code

		procDirectory = Paths.get(procDirectory.toString(), "/stat");
		List<String> list = new ArrayList<>();

		try {
			for (String a : Files
					.readAllLines(procDirectory)) {
				if (a.toString().startsWith("cpu")) {
					list.add(a);
				}
			}
		} catch (IOException e) {
			System.out.println("Error: at parseCPUOverallTicks --> readLines");
			return uptimeClockTicks;		// 0 ticks
		}

		Iterator<String> stringIterator = list.iterator();
		while (stringIterator.hasNext()) {
			String[] arr = stringIterator.next().split("\\s+");
			uptimeClockTicks = uptimeClockTicks + Arrays.stream(arr)
					.filter(a -> a.matches("-?\\d+"))
					.mapToLong(Long::parseLong)
					.reduce(0L, Long::sum);
		}
		// ----------------------------------------------------------------------

		return uptimeClockTicks;
	}
}
