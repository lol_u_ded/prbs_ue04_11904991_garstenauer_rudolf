package at.jku.prbs.procexp;

import java.util.Enumeration;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.plaf.FontUIResource;

import at.jku.prbs.procexp.ui.MainFrame;

public class SimpleProcessExplorer {

	private static final float FONTSCALING = 1.8f;

	public void start() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				JFrame frame = new MainFrame("PRBS Process Explorer");

				frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
				frame.setVisible(true);
				frame.setSize(900, 500);
			}
		});
	}

	public static void setUIDefaultFontScaling(float scalingFactor) {
        Enumeration<Object> keys = UIManager.getDefaults().keys();
        while (keys.hasMoreElements()) {
            Object key = keys.nextElement();
            Object value = UIManager.get(key);
            if (value instanceof FontUIResource) {
                UIManager.put(key, new FontUIResource(
                		((FontUIResource)value).deriveFont(
                				((FontUIResource)value).getSize2D() * scalingFactor)));
            }
        }
    }
	
	public static void main(String[] args) {
		SimpleProcessExplorer app = new SimpleProcessExplorer();

		setUIDefaultFontScaling(FONTSCALING);
		app.start();
	}
}
