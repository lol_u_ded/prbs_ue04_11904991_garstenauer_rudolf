package at.jku.prbs.procexp.data;

import java.nio.file.Path;

/**
 * Class that contains the status information of a process.
 */
public class ProcessInformation {
	
	/** Process information directory path */
	private final Path processInformationDirPath;

	/** ID of the process */
	private final int processID;

	/** ID of the parent process */
	private int parentProcessID;

	/** Name of the process */
	private String processName;
	
	/** Path to the executable file */
	private String executableLocation;

	/** Current working directory of the process */
	private String currentWorkingDir;

	/** Owner of the process */
	private int owner;
	
	/** Process status */
	private String status;
	
	/** Currently used memory */
	private long allocatedMemory;
	
	/** Used CPU clock ticks */
	private long overallClockTicks;

	/** Relative clock ticks since last setting clock ticks */
	private long relativeClockTicks;

	/** Current CPU usage (as percentage) */
	private double cpuUsage;

	/**
	 * To construct a ProcessInformation you need all param's.
	 * 
	 * @param processID
	 *            Process ID
	 * @param processInformationDirPath
	 *            Directory path in the proc file system
	 */
	public ProcessInformation(int processID, Path processInformationDirPath) {
		this.processInformationDirPath = processInformationDirPath.toAbsolutePath();
		this.processID = processID;
		this.overallClockTicks = 0L;
	}

	public Path getProcessInformationDirPath() {
		return processInformationDirPath;
	}

	//public ProcessInformation setProcessInformationDirPath(Path processInformationDirPath) {
	//	this.processInformationDirPath = processInformationDirPath.toAbsolutePath();
	//	return this;
	//}
	
	public int getProcessID() {
		return processID;
	}

	//public ProcessInformation setProcessID(int processID) {
	//	this.processID = processID;
	//	return this;
	//}

	public int getParentProcessID() {
		return parentProcessID;
	}

	public void setParentProcessID(int parentProcessID) {
		this.parentProcessID = parentProcessID;
	}

	public String getProcessName() {
		return processName;
	}

	public ProcessInformation setProcessName(String processName) {
		this.processName = processName;
		return this;
	}

	public String getExecutableLocation() {
		return executableLocation;
	}

	public ProcessInformation setExecutableLocation(String executableLocation) {
		this.executableLocation = executableLocation;
		return this;
	}

	public String getCWD() {
		return currentWorkingDir;
	}
	
	public void setCWD(String currentWorkingDir) {
		this.currentWorkingDir = currentWorkingDir;
	}

	public int getOwner() {
		return owner;
	}

	public ProcessInformation setOwner(int owner) {
		this.owner = owner;
		return this;
	}

	public String getStatus() {
		return status;
	}

	public ProcessInformation setStatus(String status) {
		this.status = status;
		return this;
	}

	public long getAllocatedMemory() {
		return allocatedMemory;
	}

	public ProcessInformation setAllocatedMemory(long allocatedMemory) {
		this.allocatedMemory = allocatedMemory;
		return this;
	}

	public long getClockTicksIncrease() {
		return relativeClockTicks;
	}

	public long getOverallClockTicks() {
		return overallClockTicks;
	}

	public ProcessInformation setOverallClockTicks(long overallClockTicks) {
		this.relativeClockTicks = (overallClockTicks - this.overallClockTicks);
		this.overallClockTicks = overallClockTicks;
		return this;
	}

	public double getCPUUsage() {
		return cpuUsage;
	}

	public void setCPUUsage(double cpuUsage) {
		this.cpuUsage = cpuUsage;
	}

	@Override
	public String toString() {
		return "{" +
			   "\"processName\": \"" + processName + "\"" +
			   ", \"processID\": " + processID +
			   ", \"executableLocation\": \"" + executableLocation + "\"" +
			   ", \"cwd\": \"" + currentWorkingDir + "\"" +
			   ", \"owner\": " + owner +
			   ", \"status\": \"" + status + "\"" +
			   ", \"allocatedMemory\": " + allocatedMemory +
			   ", \"overallClockTicks\": " + overallClockTicks +
			   "}";
	}
}
