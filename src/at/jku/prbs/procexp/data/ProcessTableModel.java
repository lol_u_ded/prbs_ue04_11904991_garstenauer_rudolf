package at.jku.prbs.procexp.data;

import javax.swing.table.AbstractTableModel;

public class ProcessTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 4445803601222487311L;

	protected final static String COLUMN_PROCESS_NAME        = "Process Name";
	protected final static String COLUMN_PROCESS_ID          = "Process ID";
	protected final static String COLUMN_PARENTPROCESS_ID    = "Parent PID";
	protected final static String COLUMN_EXECUTABLE_LOCATION = "Executable File";
	protected final static String COLUMN_WORKING_DIRECTORY   = "Working Directory";
	protected final static String COLUMN_OWNER               = "Process Owner";
	protected final static String COLUMN_STATUS              = "Status";
	protected final static String COLUMN_MEMORY_USAGE        = "Virtual Memory Size";
	protected final static String COLUMN_CPU_USAGE           = "CPU Usage";

	// Order of the process information columns
	protected final static String[] COLUMN_ORDER = {
		COLUMN_PROCESS_NAME,
		COLUMN_PROCESS_ID,
		COLUMN_PARENTPROCESS_ID,
		COLUMN_CPU_USAGE,
		COLUMN_MEMORY_USAGE,
		COLUMN_OWNER,
		COLUMN_STATUS,
		COLUMN_EXECUTABLE_LOCATION,
		COLUMN_WORKING_DIRECTORY,
	};

	private ProcessInformation[] storedData = null;

	public void updateData(ProcessInformation[] data) {
		storedData = data;
		fireTableDataChanged();
	}

	@Override
	public int getColumnCount() {
		return COLUMN_ORDER.length;
	}

	@Override
	public int getRowCount() {
		return (storedData != null) ? storedData.length : 0;
	}

	@Override
	public Object getValueAt(int rowNumber, int colNumber) {
		if (storedData == null) { return null; }
		//if ((rowNumber < 0) && (rowNumber >= storedData.length)) { return null; }
		//if ((colNumber < 0) && (colNumber >= COLUMN_ORDER.length)) { return null; }
		
		ProcessInformation row = storedData[rowNumber];
		return getCellByColumnName(row, COLUMN_ORDER[colNumber]);
	}

	private Object getCellByColumnName(ProcessInformation row, String columnName) {
		switch (columnName) {
			case COLUMN_PROCESS_NAME:
				return row.getProcessName();
				
			case COLUMN_PROCESS_ID:
				return row.getProcessID();
				
			case COLUMN_PARENTPROCESS_ID:
				return row.getParentProcessID();
				
			case COLUMN_EXECUTABLE_LOCATION:
				return row.getExecutableLocation();

			case COLUMN_WORKING_DIRECTORY:
				return row.getCWD();

			case COLUMN_OWNER:
				return row.getOwner();

			case COLUMN_STATUS:
				return row.getStatus();

			case COLUMN_CPU_USAGE:
				return new PercentageColumnType(row.getCPUUsage());

			case COLUMN_MEMORY_USAGE:
				return new MemoryUnitColumnType(row.getAllocatedMemory());

			default:
				return "n/a";
		}
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	@Override
	public String getColumnName(int column) {
		return COLUMN_ORDER[column];
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (COLUMN_ORDER[columnIndex]) {
			case COLUMN_PROCESS_NAME:
			case COLUMN_STATUS:
			case COLUMN_EXECUTABLE_LOCATION:
			case COLUMN_WORKING_DIRECTORY:
				return String.class;
				
			case COLUMN_PROCESS_ID:
			case COLUMN_PARENTPROCESS_ID:
			case COLUMN_OWNER:
				return Integer.class;
				
			case COLUMN_CPU_USAGE:
				return PercentageColumnType.class;
	
			case COLUMN_MEMORY_USAGE:
				return MemoryUnitColumnType.class;
	
			default:
				return Object.class;
		}
	}
	
	private static class PercentageColumnType implements Comparable<PercentageColumnType> {
		public final double value;
		public final String formattedValue;
		
		public PercentageColumnType(double value) {
			this.value = value;
			this.formattedValue = String.format("%.2f %%", value);
		}
		
		@Override
		public int compareTo(PercentageColumnType obj) {
			if (obj == null) { return -1; }
			//if (this == obj) { return 0; }
			
			if (this.value == obj.value) {
				return 0;
			} else if (this.value < obj.value) {
				return -1;
			} else {
				return 1;
			}
		}
		
		@Override
		public boolean equals(Object obj) {
			if (obj == null) { return false; }
			if (this == obj) { return true; }
			
			if (obj instanceof PercentageColumnType) {
				if (this.value == ((PercentageColumnType)obj).value) {
					return true;
				}
			}
			
			return false;
		}

		@Override
		public String toString() {
			return formattedValue;
		}
	}
	
	private static class MemoryUnitColumnType implements Comparable<MemoryUnitColumnType> {
		public final long value;
		public final String formattedValue;
		
		public MemoryUnitColumnType(long value) {
			this.value = value;
			if (value > (1024 * 1024 * 1024)) {
				this.formattedValue = String.format("%.2f GiB", (double)value / (double)(1024 * 1024 * 1024));
			} else if (value > 1024 * 1024) {
				this.formattedValue = String.format("%.2f MiB", (double)value / (double)(1024 * 1024));
			} else if (value > 1024) {
				this.formattedValue = String.format("%.2f KiB", (double)value / (double)1024);
			} else {
				this.formattedValue = String.format("%.2f B", (double)value);
			}
		}
		
		@Override
		public int compareTo(MemoryUnitColumnType obj) {
			if (obj == null) { return -1; }
			//if (this == obj) { return 0; }
			
			if (this.value == obj.value) {
				return 0;
			} else if (this.value < obj.value) {
				return -1;
			} else {
				return 1;
			}
		}
		
		@Override
		public boolean equals(Object obj) {
			if (obj == null) { return false; }
			if (this == obj) { return true; }
			
			if (obj instanceof PercentageColumnType) {
				if (this.value == ((PercentageColumnType)obj).value) {
					return true;
				}
			}
			
			return false;
		}

		@Override
		public String toString() {
			return formattedValue;
		}
	}
}
