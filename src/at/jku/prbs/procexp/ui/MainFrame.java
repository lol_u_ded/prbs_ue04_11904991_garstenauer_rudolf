package at.jku.prbs.procexp.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

import at.jku.prbs.procexp.ProcessDataManager;
import at.jku.prbs.procexp.data.ProcessTableModel;

public class MainFrame extends JFrame {

	private static final long serialVersionUID = -6916529642261161449L;
	
	private ProcessTableModel dataModel;
	private ProcessDataManager processListManager;

	public MainFrame(String title) throws HeadlessException {
		super(title);

		processListManager = new ProcessDataManager();

		constructUI();
	}

	private void constructUI() {
		setLayout(new BorderLayout());

		dataModel = new ProcessTableModel();

		// Table that presents process information
		JTable displayedTable = new JTable(dataModel);
		displayedTable.setPreferredScrollableViewportSize(new Dimension(500, 200));
		displayedTable.setFillsViewportHeight(true);
		displayedTable.setRowHeight(10 + (int)(displayedTable.getFont().getSize2D() * 1.2f));
		displayedTable.setIntercellSpacing(new Dimension(10, 0));
		displayedTable.setAutoResizeMode(JTable.AUTO_RESIZE_NEXT_COLUMN);
		displayedTable.setAutoCreateRowSorter(true);

		// Initialize data
		dataModel.updateData(processListManager.updateCurrentProcessInformation());

		JScrollPane scrollPane = new JScrollPane(displayedTable);

		add(scrollPane, BorderLayout.CENTER);

		// To refresh the table
		JButton reloadProcInfoTable = new JButton("Reload");
		// To clear everything that's shown in the table
		JButton clearProcInfoTable = new JButton("Clear table");
		// To save the current process map
		JButton saveProcInfo = new JButton("Save process information");

		JPanel buttonPanel = new JPanel(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.weightx = 0.25;
		constraints.weighty = 1.0;
		constraints.fill = GridBagConstraints.BOTH;
		buttonPanel.add(reloadProcInfoTable, constraints);
		buttonPanel.add(clearProcInfoTable, constraints);
		
		constraints.gridwidth = 2;
		constraints.weightx = 0.5;
		buttonPanel.add(saveProcInfo, constraints);
		
		add(buttonPanel, BorderLayout.SOUTH);
		buttonPanel.setPreferredSize(
				new Dimension(buttonPanel.getPreferredSize().width,
				              (int)(buttonPanel.getPreferredSize().height * 1.5f)));

		/**
		 * Button to reload process information table
		 */
		reloadProcInfoTable.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						dataModel.updateData(processListManager.updateCurrentProcessInformation());
					}
				});
			}
		});

		/**
		 * Button to clear process information table.
		 */
		clearProcInfoTable.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						// clear by setting an empty data "array"
						dataModel.updateData(null);
					}
				});
			}
		});

		/**
		 * Button to save the current process information into a file
		 */
		saveProcInfo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				final JFileChooser fc = new JFileChooser();
				int returnVal = fc.showOpenDialog(MainFrame.this);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					// Get the selected path and save the process information
					processListManager.storeProcessInformationToFile(fc.getSelectedFile().toPath());
				}
			}
		});
	}
}
