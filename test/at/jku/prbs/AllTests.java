package at.jku.prbs;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import at.jku.prbs.procexp.ProcessParserTester;
import at.jku.prbs.procexp.ProcessDataManagerTester;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	ProcessParserTester.class,
	ProcessDataManagerTester.class,
})

public class AllTests {}
