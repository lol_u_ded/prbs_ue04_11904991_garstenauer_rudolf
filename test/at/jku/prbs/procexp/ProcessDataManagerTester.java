package at.jku.prbs.procexp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

import at.jku.prbs.procexp.ProcessDataManager;
import at.jku.prbs.procexp.data.ProcessInformation;

public class ProcessDataManagerTester extends ProcessExplorerTest {

	private ProcessDataManager testProcessManager;
	
	@Before
	public void setUp() throws Exception {
		super.setUp();
		
		testProcessManager = new ProcessDataManager();
	}
	
	@Test
	public void testSaveToDisk() throws IOException {
		testProcessManager.updateCurrentProcessInformation(rootPath);
		
		ProcessInformation[] pi = testProcessManager.getAllProcessesInfo();
		
		assertNotEquals(null, pi);
		assertEquals(ProcessParserTester.NUMBER_OF_PROCESSES, pi.length);
		
		Path fileStorageLocation = Paths.get("testfile-savetodisk1");
		testProcessManager.storeProcessInformationToFile(fileStorageLocation);

		assertTrue(Files.isRegularFile(fileStorageLocation));
		assertTrue(Files.size(fileStorageLocation) > 0);
		
		Files.deleteIfExists(fileStorageLocation);
	}

	@Test
	public void testSaveToDiskOverwritingNotAppending() throws IOException {
		testProcessManager.updateCurrentProcessInformation(rootPath);

		ProcessInformation[] pi = testProcessManager.getAllProcessesInfo();
		
		assertNotEquals(null, pi);
		assertEquals(ProcessParserTester.NUMBER_OF_PROCESSES, pi.length);
		
		Path fileStorageLocation = Paths.get("testfile-savetodisk2");
		testProcessManager.storeProcessInformationToFile(fileStorageLocation);

		long size = Files.size(fileStorageLocation);

		// Save again at the same location
		testProcessManager.storeProcessInformationToFile(fileStorageLocation);
		
		// Should stay the same size
		assertEquals(size, Files.size(fileStorageLocation));
		
		Files.deleteIfExists(fileStorageLocation);
	}
}
