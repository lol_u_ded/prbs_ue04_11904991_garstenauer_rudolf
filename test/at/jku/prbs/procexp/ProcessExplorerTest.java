package at.jku.prbs.procexp;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Before;

public abstract class ProcessExplorerTest {

	// ----------------------------------------------------------------------
	// 
	// TODO: Set path to the test file system tree. Use the unpacked tree from
	//       PRBS_UE_04_TestProcFSTree.tar.gz.
	//protected static final String TEST_FS_PATH = "<PATH TO TEST FILE SYSTEM>";
	protected static final String TEST_FS_PATH = "/home/user1/eclipse-workspace/testProcFSTree/";
	
	protected Path rootPath;

	public ProcessExplorerTest() {
		super();
	}

	@Before
	public void setUp() throws Exception {
		try {
			assertNotEquals("Path to the test procfs tree not set. Set the constant ProcessExplorerTest.TEST_FS_PATH to the correct directory!",
					"<PATH TO TEST FILE SYSTEM>", TEST_FS_PATH);
			
			Path p = Paths.get(TEST_FS_PATH);
			assertTrue("Path of the test procfs file system tree does not exist! Set the constant ProcessExplorerTest.TEST_FS_PATH to a valid directory!",
					Files.exists(p) && Files.isDirectory(p));
			
			rootPath = Paths.get(TEST_FS_PATH).toAbsolutePath().normalize();
		} catch(InvalidPathException e){
			fail("Path to the procfs test file system tree not set. Set the constant ProcessExplorerTest.TEST_FS_PATH to the correct directory!");
		}
	}
}
