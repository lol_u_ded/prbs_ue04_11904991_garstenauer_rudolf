package at.jku.prbs.procexp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;

import org.junit.Test;

import at.jku.prbs.procexp.data.ProcessInformation;

public class ProcessParserTester extends ProcessExplorerTest {

	public static final int WEBCONTENT_PID = 1320;
	public static final int BROWSER_PID = 1356;
	public static final int BROWSER_PPID = 1;
	public static final int BROWSER_OWNER = 1000;
	public static final String BROWSER_STATE = "S (sleeping)";
	public static final long BROWSER_MEMORY = 805896192;
	public static final long BROWSER_TOTALTICKS = 2347;
	public static final long CPU_OVERALL_TICKS = 317555;
	public static final int NUMBER_OF_PROCESSES = 115;
	
	@Test
	public void testGetProcessDirectories() {
		Path[] processIDs = ProcessParser.getAllProcessIDPaths(rootPath);

		assertNotEquals(null, processIDs);
		
		assertEquals(NUMBER_OF_PROCESSES, processIDs.length);
		
		for (Path p : processIDs) {
			assertTrue("Entries in the list of processes have to be directories", Files.isDirectory(p));
		}
	}

	@Test
	public void testProcessInformationHashMap() {
		HashMap<Integer, ProcessInformation> pd = ProcessParser.parseProcessDetails(rootPath, null);

		assertNotEquals(null, pd);
		
		assertEquals(NUMBER_OF_PROCESSES, pd.size());
	}

	@Test
	public void testPIDParsing() {
		ProcessInformation browser = parseDetailsAndReturnProcess(BROWSER_PID);
		
		// Browser exists in HashMap
		assertNotEquals(null, browser);
		
		assertEquals(BROWSER_PID, browser.getProcessID());
	}

	@Test
	public void testNameParsingWithSpace() {
		ProcessInformation browser = parseDetailsAndReturnProcess(WEBCONTENT_PID);
		
		assertNotEquals(null, browser);
		
		assertEquals("Web Content", browser.getProcessName());
	}


	@Test
	public void testPPIDParsing() {
		ProcessInformation browser = parseDetailsAndReturnProcess(BROWSER_PID);
		
		assertNotEquals(null, browser);
		
		assertEquals(BROWSER_PPID, browser.getParentProcessID());
	}

	@Test
	public void testOwnerIDParsing() {
		ProcessInformation browser = parseDetailsAndReturnProcess(BROWSER_PID);
		
		assertNotEquals(null, browser);
		
		assertEquals(BROWSER_OWNER, browser.getOwner());
	}
	
	@Test
	public void testStatusParsing() {
		ProcessInformation browser = parseDetailsAndReturnProcess(BROWSER_PID);
		
		assertNotEquals(null, browser);
		
		assertEquals(BROWSER_STATE, browser.getStatus());
	}

	@Test
	public void testMemoryParsing() {
		ProcessInformation browser = parseDetailsAndReturnProcess(BROWSER_PID);
		
		assertNotEquals(null, browser);
		
		assertEquals(BROWSER_MEMORY, browser.getAllocatedMemory());
	}

	@Test
	public void testOverallTicksParsing() {
		ProcessInformation browser = parseDetailsAndReturnProcess(BROWSER_PID);
		
		assertNotEquals(null, browser);
		
		assertEquals(BROWSER_TOTALTICKS, browser.getOverallClockTicks());
	}

	@Test
	public void testRelativeTicksParsing() {
		HashMap<Integer, ProcessInformation> pd = ProcessParser.parseProcessDetails(rootPath, null);
		
		assertNotEquals(null, pd);
		
		ProcessInformation browser = pd.get(BROWSER_PID);
		
		assertNotEquals(null, browser);
		assertEquals(BROWSER_TOTALTICKS, browser.getClockTicksIncrease());
		
		// Update process details and check if relative ticks are 0 now
		pd = ProcessParser.parseProcessDetails(rootPath, pd);
		browser = pd.get(BROWSER_PID);
		
		// Does this test really make any sense???
		assertEquals(0, browser.getClockTicksIncrease());
	}

	@Test
	public void testBinaryLocationParsing() throws IOException {
		ProcessInformation browser = parseDetailsAndReturnProcess(BROWSER_PID);
		
		assertNotEquals(null, browser);

		assertNotEquals(null, browser.getCWD());
		
		// check if exe path is set correctly (note that in an actual system this should be a valid link)
		Path exeFileLink = rootPath.resolve(String.valueOf(BROWSER_PID)).resolve("exe");
		Path exeFilePath = rootPath.toRealPath().resolve(String.valueOf(BROWSER_PID)).resolve("testbinary");
		
		assertTrue("Path to EXE is not a symlink! Make sure that the test procfs tree was properly extracted and that the target file system supports symbolic links! This test will fail on Windows or if the test FS was extracted to a VirtualBox shared folder!",
				Files.isSymbolicLink(exeFileLink));
		
		assertEquals("Path to EXE not canonicalized!", exeFilePath.toString(), browser.getExecutableLocation());
	}

	@Test
	public void testCWDParsing() throws IOException {
		ProcessInformation browser = parseDetailsAndReturnProcess(BROWSER_PID);
		
		assertNotEquals(null,browser);
		
		assertNotEquals(null,browser.getCWD());
		
		// check if cwd path is set correctly (note that in an actual system this should be a valid link)
		Path cwdFileLink = rootPath.resolve(String.valueOf(BROWSER_PID)).resolve("cwd");
		Path cwdFilePath = rootPath.toRealPath().resolve(String.valueOf(BROWSER_PID)).resolve("testcwd");
		
		assertTrue("Path to CWD is not a symlink! Make sure that the test procfs tree was properly extracted and that the target file system supports symbolic links! This test will fail on Windows or if the test FS was extracted to a VirtualBox shared folder!",
				Files.isSymbolicLink(cwdFileLink));
		
		assertEquals("Path to CWD not canonicalized!", cwdFilePath.toString(), browser.getCWD());
	}

	@Test
	public void testCPUOverallTicksParsing() {
		long cpuOverallTicks = ProcessParser.parseCPUOverallTicks(rootPath);
		
		assertEquals(CPU_OVERALL_TICKS, cpuOverallTicks);
	}
	
	private ProcessInformation parseDetailsAndReturnProcess(int pid) {
		HashMap<Integer, ProcessInformation> pd = ProcessParser.parseProcessDetails(rootPath, null);
		
		if (pd == null) { return null; }
		
		return pd.get(pid);
	}
}
